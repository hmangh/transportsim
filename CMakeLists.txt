cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(MyProject)

enable_language(Fortran)

if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
    set(CMAKE_Fortran_FLAGS         "${CMAKE_Fortran_FLAGS}")
    set(CMAKE_Fortran_FLAGS_DEBUG   "-O0 -g3 -fbacktrace -fcheck=all")
    set(CMAKE_Fortran_FLAGS_RELEASE "-Ofast -march=native")
endif()

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

# this is where we will place the Fortran module files
set(CMAKE_Fortran_MODULE_DIRECTORY ${PROJECT_BINARY_DIR}/modules)
#AUX_SOURCE_DIRECTORY(dir *.f90)
file(GLOB SOURCES "src/*.f90" "bspline-lib/*.f90")
# the executable is built from 3 source files
add_executable(
    run.x
    ${SOURCES}
    )
