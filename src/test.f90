program test
  use parameters
  use ions_data
  use monte_carlo
  implicit none

  integer :: i
  integer :: charge
  integer :: charge_series(3)
  integer :: icharge, qindx, qsize
  integer :: init_q(3)
  real(dp):: energy
  type(ion_type) :: ion(min_charge:charge_num)

  call read_initiate_data

 ! call interpolation_test(1); stop

  !write(*, '(A)', ADVANCE = "NO") "Enter the energy (KeV/u):  "
  !read(*,*)energy
  charge_series = [-1,0,1]
  do i=2,2
     energy = energylist(i)
     call stopping_power(energy,charge_series)
  end do
  !******************************************************************************
  !************************TESTS************************************************
  !******************************************************************************
contains
  subroutine interpolation_test(charge)

    integer :: charge, max_chan_num, i, j

    real(dp), dimension (44) :: e_list &
      = [1.d0,2.d0, 3.d0, 4.d0, 5.d0, 8.d0, 10.d0, 12.d0, 20.d0, 25.d0, &
      30.d0, 35.d0, 40.d0, 45.d0, 50.d0, 55.d0, 60.d0, 65.d0, 70.d0, 75.d0, &
      80.d0, 85.d0, 90.d0, 100.d0, 125.d0, 150.d0, 200.d0, 250.d0, 300.d0,  &
      400.d0, 500.d0, 600.d0, 750.d0, 800.d0, 900.d0, 1.d3, 1.5d3,   &
      2.d3, 2.5d3, 5.d3, 8.d3, 1.d4, 2.d4, 2.5d4 ]

    character (len=128) :: fmt, fmt1, fmt2, filename

    type(ion_type) :: ion

    ion = initial_ion(charge, 2.5d4)
    max_chan_num =  size(ion%channels)
    write(fmt, '("(F6.0,",I0,"(E15.2,F15.2))")') max_chan_num
    write(fmt1,'("(6x,",I0,"(14x,A2,A1,14x))")') max_chan_num
    write(fmt2, '("(F6.0,A1,",I0,"(E15.3,A1,F15.3,A1))")') max_chan_num
    write(filename, '(A14,A14,I1,A4)') outputfolder,"interpolation_",ion%charge,".csv"
    open(unit=1,file=trim(filename))
    write(*,'(A100)')"===================================================================",&
      "===================================================================",&
      "====================="
    write(*,fmt1)(ion%channels(j)%symbol,j=1,max_chan_num)
    write(*,'(A100)')"===================================================================",&
      "===================================================================",&
      "====================="

    write(1,fmt1)(ion%channels(j)%symbol,",",j=1,max_chan_num)
    do i = 1, size(e_list)


      call transport_ion(ion,charge, e_list(i),'bspline')

      ! ion%energy=e_list(i)
      !
      !  if(ion%energy<=e_list(size(e_list)-1) .and.ion%energy>=e_list(4) ) then
      !    call spline_interpolate(ion)
      !  else
      !    call line_interpolate(ion)
      !  end if

      ! do j = 1, max_chan_num
      !   write(*,*)ion%main_process(j)%symbol
      write(*,fmt) e_list(i),(ion%channels(j)%csx, &
        ion%channels(j)%enr_loss,j=1,max_chan_num)
      !   end do
      write(1,fmt2) e_list(i),",",(ion%channels(j)%csx,",", &
        ion%channels(j)%enr_loss,",",j=1,max_chan_num)
    end do

  end subroutine


  end
