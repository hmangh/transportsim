module monte_carlo
  use parameters
  use ions_data
  implicit none
  private
  public stopping_power

  real(dp) :: stopenergy

contains

  subroutine random_walk(ion, charge, energy, length, flag)
    type(ion_type) :: ion
    real(dp) :: length
    real(dp), allocatable :: lambda(:)
    real(dp) :: minlambda
    real(dp) :: energy
    real(dp) :: initial_energy
    real(dp) :: distrib
    integer  :: flag
    integer  :: initial_charge
    integer  :: chan_size
    integer  :: minarray(1)
    integer  :: min_indx
    integer  :: charge
    integer  :: ichan
    !!  Save the incoming ion charge & energy
    initial_charge = charge
    initial_energy = energy

    chan_size = size(ion%channels)

    allocate(lambda(chan_size))

    do ichan = 1, chan_size
      distrib = rnunf()
      lambda(ichan) = -log(1.0_dp - distrib)/densnum
      lambda(ichan) = lambda(ichan) / ion%channels(ichan)%csx
    end do

    minlambda=minval(lambda)!; print*,minlambda
    minarray=minloc(lambda)
    min_indx = minarray(1)

    energy = energy*conv_fac - ion%channels(min_indx)%enr_loss
    energy = energy/conv_fac

    !if(energy<stopenergy)then
    !  energy = initial_energy
    !  charge = initial_charge
    !  flag = 1
    !else
    !  ion%energy = energy
      length = length + minlambda
      charge = charge + ion%channels(min_indx)%charge_addition
    !endif
    !print*,'energy:', energy, stopenergy
    if(length>=upperlength.OR.energy<=stopenergy.OR.energy<=0.d0) then
      flag=1
    end if

  end subroutine


  subroutine stopping_power(start_E,chg_series)
    type(ion_type)        :: ion(num_charge_channels)
    real(dp), allocatable :: interval(:), mean(:), sig(:)
    real(dp), allocatable :: intv_length(:,:,:)
    real(dp), allocatable :: stoppwr(:,:),lavg(:,:), charge_bin(:,:)
    real(dp), allocatable :: charge_bin_percent(:,:),chansum(:,:)
    real(dp) :: start_E
    real(dp) :: length
    real(dp) :: energy
    real(dp) :: sum_charge
    real(dp) :: sig_length
    real(dp) :: Equi_charge
    integer  :: chg_series(:)
    integer  :: ncounted
    integer  :: intv_size
    integer  :: charge
    integer  :: flag
    integer  :: qsize, qindx
    integer  :: start_charge
    integer  :: icharge, itraj,i,j,k
    integer  :: chgfirst, chglast, chg_1_indx, chg_2_indx, chg(1)
    character (len=128) :: fmt24
    character (len=128) :: filename
    character (len=3)   :: qlist(3)=['q=-','q=0','q=1']
    logical             :: filechk

    !initialize an ion in each charge channel
    do icharge = 1, num_charge_channels

      charge       = indx_charge(icharge)
      print*, charge
      ion(icharge) = initial_ion(charge, start_E)

    end do

    !      start_E = ion(1)%energy
    write(*,*)"Initial Energy: ", start_E, " KeV/u"
    !
    !chg = lbound(chg_series)
    chgfirst = minval(chg_series)
    !chg  = ubound(chg_series)
    chglast  = maxval(chg_series)

    chg_1_indx = charge_indx(chgfirst)

    chg_2_indx = charge_indx(chglast)

    qsize= size(chg_series)

    if(chg_2_indx > num_charge_channels) then
            print*, 'charge:',chglast
            stop 'wrong charge entered '
    end if        
    !
    print*, chgfirst, chglast, chg_1_indx, chg_2_indx
   
    call read_energy_intervals(start_E, interval)
    !
    intv_size = size(interval)
    !
    allocate(charge_bin(intv_size, num_charge_channels ))
    allocate(intv_length(intv_size,ntraj,chg_1_indx:chg_2_indx))
    allocate(mean(intv_size),sig(intv_size))
    allocate(stoppwr(intv_size, chg_1_indx:chg_2_indx))
    allocate(lavg(intv_size, chg_1_indx:chg_2_indx))
    allocate(chansum(num_charge_channels,intv_size))
    allocate(charge_bin_percent(intv_size,num_charge_channels))
    !

    stopenergy = start_E - interval(intv_size)
    !
    if(stopenergy<1.d0 )then
            write(*,*)"Intervals are too large for given energy"
            stop
    end if
    !=====================================================================
    !====Loop over ion charges
          do icharge =chg_1_indx, chg_2_indx
           ! print*, 'icharge:',icharge
            start_charge = indx_charge(icharge)
            print*,'start_charge', start_charge
            sig_length = 0.0_dp
            intv_length = 0.0_dp
            !xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            !xx loop over different trajectories
            do itraj = 1, ntraj
    !
              qindx = icharge
              ion(qindx)%energy = start_E
              ion(qindx)%charge = start_charge
              energy  = start_E
              charge = start_charge
              length = 0.0_dp
              flag   = 0
              j      = 1
    !          !***********************************************************************
    !          !***Loop over interactions until the end/stop flag (flag==1) is raised
              do while (flag == 0)
    !
               ! call transport_ion(ion(qindx),charge,energy,'spline')
                call transport_ion(ion(qindx),charge,energy,'bspline')
    !
                call random_walk(ion(qindx), charge, energy, length, flag)
    !            print*, charge
    !
                qindx = charge_indx(charge)
    !
    !            print*, 'flag:',flag, energy, stopenergy
    !            print*, qindx
                if(energy <= start_E - interval(j).and. flag==0)then
                  intv_length(j,itraj,icharge)=length
                  !print*, 'intv_l:',intv_length(j, itraj, icharge)
                  charge_bin(j,qindx) = charge_bin(j,qindx) + 1
                  j = j + 1
                end if
                !print*, j
              end do !do while
                  intv_length(j,itraj,icharge)=length
                  !print*, 'intv_l:',intv_length(j, itraj, icharge)
                  charge_bin(j,qindx) = charge_bin(j,qindx) + 1
                            
            !*********************************************************************
            end do !itraj
            !xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            ncounted = ntraj
            do j=1,intv_size
              sig_length=0.d0
              sum_charge = 0
              do i=1,num_charge_channels
                sum_charge = sum_charge + charge_bin(j,i)
              end do
              charge_bin_percent(j,:) = charge_bin(j,:)/dble(sum_charge)*100.d0
              do k=1,ntraj
                sig_length=sig_length+intv_length(j,k,icharge)
    !            if(intv_length(j,k,icharge)==0.d0)then
    !              ncounted = ncounted - 1
    !            end if
              end do
              !** calculate stopping power
    !          print*, j, icharge, sig_length, ncounted
              lavg(j,icharge)=sig_length/ncounted
    !          !print*,sig_length,lavg(j,l)
              stoppwr(j,icharge)=Real(atm_num)*interval(j)/1000.0/(lavg(j,icharge))/&
                (2.0d3*densnum/avogadro_num)
            end do
    !
          end do !icharge
    !      !===============================================================================
    filename = outputfolder//"SP_intv.out"
    inquire(file=trim(filename), exist=filechk)
    if (filechk) then
        open(24, file=trim(filename), status="old", position="append", action="write")
    else
        open(24, file=trim(filename), status="new", action="write")
        
          write(fmt24,'("(A11,x,A11,x,",I0,"(2x,A6),2(2x,A6)",I0,"(2x,A6))")')qsize, qsize +1
          write(24,*)'stopping powers in Mev.cm^2/mg and equilibrium charges for :'
          write(24,'(A100)')'============================================================'//&
                  '==========================================='
          write(24,fmt24)'E(KeV/u)','dE(KeV/u)',qlist(chg_1_indx:chg_2_indx),'Avg','sigma', &
                         qlist(:), 'Equilibrium'               
          write(24,'(A100)')'============================================================'//&
                  '==========================================='
    end if      
    
    
    

          do j=1,intv_size

           mean(j)=sum(stoppwr(j,:))/(qsize)
            sig(j) = 0.d0
            do i=chg_1_indx, chg_2_indx
              sig(j)=sig(j)+(stoppwr(j,i)-mean(j))**2
            end do

            sig(j)=sqrt(sig(j)/max(qsize-1,1))

            Equi_charge=0
            do icharge=1,num_charge_channels
                 charge = indx_charge(icharge)
                 Equi_charge=Equi_charge+charge_bin_percent(j,icharge)*dble(charge)
           end do
           Equi_charge=Equi_charge/100.0
           !write(*,*)intv(j), charge_bin_percent(j,:), Equi_charge
           if(sig(j) < 0.0055d0) then
               write(24,'(x,F9.3,7x,F8.3,6x,9(2x,F7.3))')start_E,start_E-interval(j),stoppwr(j,:),mean(j),sig(j), &
                                                         charge_bin_percent(j,:), Equi_charge
               write(*,'(x,F9.3,7x,F8.3,6x,9(2x,F7.3))')start_E,start_E-interval(j),stoppwr(j,:),mean(j),sig(j), &
                                                         charge_bin_percent(j,:), Equi_charge
               exit
           end if
          end do
          if(j-1 == intv_size) then        
               j = j -1
              ! do icharge = chg_1_indx, chg_2_indx
              !    print*, j, interval(j),lavg(j,icharge)
              ! end do 
              if(sig(j)>= 0.0055d0) then
                  write(24,'(x,F9.3,7x,F8.3,6x,9(2x,F7.3))')start_E,start_E-interval(j),stoppwr(j,:),mean(j),sig(j),&
                                                            charge_bin_percent(j,:), Equi_charge
              end if                                    
          end if
          !close(24)
  end subroutine

  subroutine read_energy_intervals(start_E,intv)

    integer :: i, j, io, num_intv, nlines
    real(dp) :: placeholder, start_E
    real(dp), allocatable :: intv(:)
    !**determines the intervals from file
    !first reads the length of the relevant intervals
    i= 0; j= 0; io= 0
    open(unit = 3,file =inputfolder//"intv.txt",status ='OLD',iostat =io)
    do while(io == 0)
      read(3,*, iostat = io)placeholder
      j=j+1!; print*,j
    end do

    !print*,io
    rewind(unit = 3, iostat = io)
    !print*, io

    nlines=j-1   !;print*,"end line:",nlines



    do j=1,nlines!27
      read(3,*, iostat = io)placeholder
      i=i+1
      if(start_E-placeholder.lt.1.d0)then
        i=i-1
        exit
      endif
    end do
    rewind(unit = 3, iostat =io)

    !**number of relevant energy intervals to be used
    num_intv=i
    print*,"num_intv:",num_intv

    allocate(intv(num_intv))

    !***********************************************
    do j=1,num_intv
      read(3,*,iostat = io)intv(j)
      !print*,"intv(",j,"):",intv(j)
    end do

    close(3)

  end subroutine

  !****************************************************************************
  function rnunf()
    !!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ! Random number generator routine after the ran2 recipe of numerical recipes.
    !!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    INTEGER idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,NTAB,NDIV
    DOUBLE PRECISION rnunf,AM,EPS,RNMX
    PARAMETER (IM1=2147483563,IM2=2147483399,AM=1.d0/IM1,IMM1=IM1-1,&
      &IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,IR2=3791,&
      &NTAB=32,NDIV=1+IMM1/NTAB,EPS=3.d-16,RNMX=1.d0-EPS)
    INTEGER idum2,j,k,iv(NTAB),iy
    common /rands/ idum
    !      common /randsave/	nrnum
    SAVE iv,iy,idum2
    DATA idum2/123456789/, iv/NTAB*0/, iy/0/
    if (idum.le.0) then
      idum=max(-idum,1)
      idum2=idum
      do 11 j=NTAB+8,1,-1
        k=idum/IQ1
        idum=IA1*(idum-k*IQ1)-k*IR1
        if (idum.lt.0) idum=idum+IM1
        if (j.le.NTAB) iv(j)=idum
        11      continue
        iy=iv(1)
      endif
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if (idum.lt.0) idum=idum+IM1
      k=idum2/IQ2
      idum2=IA2*(idum2-k*IQ2)-k*IR2
      if (idum2.lt.0) idum2=idum2+IM2
      j=1+iy/NDIV
      iy=iv(j)-idum2
      iv(j)=idum
      if(iy.lt.1)iy=iy+IMM1
      rnunf=min(AM*iy,RNMX)
      !	     nrnum=nrnum+1

      return
    End function


  end module
