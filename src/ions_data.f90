module ions_data
        use parameters
        use bspline_module, only: bspline_1d
        use interpolation, only : spline_type
        implicit none
        private
        public ion_type, initial_ion, transport_ion, &
                read_initiate_data


        type process
                real(dp)         ::  csx, enr_loss
                character(len=:), allocatable ::  symbol
                integer          :: order
                integer          :: charge_addition
        contains
                procedure        :: find_csx_enr_loss_spline
                procedure        :: find_csx_enr_loss_bspline
        end type process

        type ion_type
                integer  :: charge
                real(dp) :: energy
                type(process), allocatable :: channels(:)
        contains
                procedure :: initialize
                procedure :: transport
        end type ion_type

        type ion_data
                integer  :: charge
                integer  :: num_channels
                integer  :: bs_order
                real(dp), allocatable :: x(:), y(:,:)
                type(bspline_1d), allocatable :: bs(:)
                type(spline_type), allocatable :: sp(:)
                !real(dp), allocatable :: a(:,:), b(:,:), c(:,:)
        end type

        type(ion_data),allocatable :: csx_data(:)
        type(ion_data),allocatable :: eloss_data(:)

contains

        !**************************************************
        !
        !**************************************************
        function initial_ion(charge, energy)
                type(ion_type) :: initial_ion
                integer        :: charge
                real(dp)      :: energy

                !*** fill charge and energy ***
                call initial_ion%initialize(charge, energy)

        end function initial_ion

        !*************************************************
        !
        !*************************************************
        subroutine transport_ion(ion,charge, energy,interp_name)
                type(ion_type) :: ion
                real(dp) :: energy
                integer  :: charge
                character(len=*) :: interp_name

                call ion%transport(charge, energy, interp_name)

        end subroutine transport_ion


        subroutine initialize(this, charge, energy)
                class(ion_type)::this
                integer :: charge
                integer :: lstr
                integer  :: j
                real(dp) :: energy
                integer, allocatable :: qplus(:)
                character(len=:),allocatable :: list(:)

                this%charge = charge
                this%energy = energy

                if(charge == 0) then
                        lstr = 2
                        list = ['SI','DI','TI','SS','SC','EX','SP','EL']!,'SX']
                        qplus = [0,0,-1,1,-1,0,0,0]!,1]
                        allocate(this%channels(chan_num(2)))
                        !allocate(character(len=lstr)::list(chan_num(2)))
                        !allocate(qplus(chan_num(2)))


                        do j= 1, chan_num(2)
                        allocate(character(len=lstr)::this%channels(j)%symbol)
                        this%channels(j)%symbol = list(j)
                        this%channels(j)%charge_addition = qplus(j)
                        end do


                elseif(charge == 1) then
                        lstr = 2
                        allocate(this%channels(chan_num(3)))
                        !allocate(character(len=lstr)::list(chan_num(3)))
                        !allocate(qplus(chan_num(3)))
                        list = ['SI','DI','TI','SC','DC','EX','EL']
                        qplus = [0,0,-1,-1,-2,0,0]
                        do j= 1, chan_num(3)
                        !allocate(character(len=lstr)::this%channels(j)%symbol)
                        this%channels(j)%symbol = list(j)
                        this%channels(j)%charge_addition = qplus(j)
                        end do


                elseif(charge == -1) then
                        lstr = 2
                        allocate(this%channels(chan_num(1)))
                        !allocate(character(len=lstr)::list(chan_num(1)))
                        !allocate(qplus(chan_num(1)))
                        list = ['SI','DI','SS','DS','EX','EL']
                        qplus = [0,0,1,2,0,0]
                        do j= 1, chan_num(1)
                        !allocate(character(len=lstr)::this%channels(j)%symbol)
                        this%channels(j)%symbol = list(j)
                        this%channels(j)%charge_addition = qplus(j)
                        end do


                else
                        print*, 'charge:', charge
                        stop "wrong charge was entered to initialize"
                end if


        end subroutine initialize


        subroutine transport(ion,charge, energy,interpolation_name)
                class(ion_type) :: ion
                integer         :: charge
                integer         :: nchn, ichan, icharge
                real(dp)       :: energy
                character(len=*) :: interpolation_name
                
               ! if(ion%charge /= charge) then
               !         print*, charge, ion%charge
               !         stop 'misfit'
               ! end if        
                ion%charge = charge
                ion%energy = energy
                icharge = charge_indx(charge) 
                nchn = size(ion%channels)
                !print*, charge, icharge, nchn

                select case(interpolation_name)
                  case('spline')
                    do ichan = 1, nchn
                     call ion%channels(ichan)%find_csx_enr_loss_spline(energy,icharge,ichan)
                    end do
                  case('bspline')
                    do ichan = 1, nchn
                     call ion%channels(ichan)%find_csx_enr_loss_bspline(energy,icharge,ichan)
                    end do
                  case default
                    print*, interpolation_name
                    stop 'name of interpolation method does not match coded methods'
                end select

        end subroutine transport
        !**************************************************
        !
        !**************************************************
        subroutine find_csx_enr_loss_spline(chan,energy,charge_order,channel_order)
                class(process) :: chan
                real(dp) :: energy
                real(dp) :: ener_log
                real(dp) :: p
                integer  :: charge_order, channel_order

                ener_log = log(energy)
                !ispline(u, x, y, b, c, d, n)
                p = csx_data(charge_order)%sp(channel_order)%evaluate(ener_log)
                chan%csx = exp (p)

                p = eloss_data(charge_order)%sp(channel_order)%evaluate(energy)
                chan%enr_loss = p


        end subroutine find_csx_enr_loss_spline


        subroutine find_csx_enr_loss_bspline(chan,energy,charge_order,channel_order)
                class(process) :: chan
                real(dp) :: energy
                real(dp) :: ener_log
                real(dp) :: p
                integer  :: charge_order, channel_order
                integer  :: idx = 0
                integer  :: iflag

                ener_log = log(energy)
                !ispline(u, x, y, b, c, d, n)
                call csx_data(charge_order)%bs(channel_order)%evaluate&
                                          (ener_log,idx,p,iflag)
                chan%csx = exp (p)
                idx = 0
                call eloss_data(charge_order)%bs(channel_order)%evaluate &
                                    (energy,idx,p,iflag)
                chan%enr_loss = p            
                !p = eloss_data(charge_order)%sp(channel_order)%evaluate(ener_log)
                !chan%enr_loss = exp(p)

        end subroutine find_csx_enr_loss_bspline


        subroutine read_initiate_data
                integer :: j
                integer :: icharge
                integer :: ienr
                integer :: ichan
                integer :: charge
                character (len = 20 ):: filename
                real(dp)             :: array(2*energy_num)


                if(.not. allocated(csx_data)) allocate(csx_data(num_charge_channels))
                if(.not. allocated(eloss_data)) allocate(eloss_data(num_charge_channels))

                do icharge = 1, num_charge_channels
                   charge = min_charge + icharge - 1
                   csx_data(icharge)%charge = charge
                   csx_data(icharge)%num_channels = chan_num(icharge)
                   allocate(csx_data(icharge)%x(energy_num))
                   allocate(csx_data(icharge)%y(energy_num,chan_num(icharge)))
                   csx_data(icharge)%x(:)= log(energylist(:))
                   allocate(csx_data(icharge)%sp(chan_num(icharge)))
                   allocate(csx_data(icharge)%bs(chan_num(icharge)))

                   eloss_data(icharge)%charge = charge
                   eloss_data(icharge)%num_channels = chan_num(icharge)
                   allocate(eloss_data(icharge)%x(energy_num))
                   allocate(eloss_data(icharge)%y(energy_num,chan_num(icharge)))
                   eloss_data(icharge)%x(:)= energylist(:)
                   allocate(eloss_data(icharge)%sp(chan_num(icharge)))
                   allocate(eloss_data(icharge)%bs(chan_num(icharge)))


                   if (charge <= 9 .and. charge >= 0 ) then
                        write(filename,"(A13,A1,I1,A4)") inputfolder,chem_sym, charge,'.dat'
                   elseif(charge==-1)then
                        write(filename,"(A13,A1,I2,A4)") inputfolder,chem_sym, charge,'.dat'
                   else
                        write(filename,"(A13,A1,I2,A4)") inputfolder,chem_sym, charge,'.dat'
                   end if

                   open (unit=1, file = trim(filename) )

                   do ichan = 1, chan_num(icharge)
                      !print*,size(ion%main_process(j)%sub_process)
                      read (1,*) array(:)
                      ienr = 0
                      do j = 1 , 2*energy_num, 2
                         ienr = ienr + 1
                         !print*, icharge, ienr, ichan, j
                         csx_data(icharge)%y(ienr,ichan) = log(array(j))
                         if(array(j)>=1.d-13) then
                              print*, array(j),'!! large cross section was read for q=', charge
                         end if
                              eloss_data(icharge)%y(ienr,ichan) = array(j+1)
                      end do !j
                  end do !ichan

                  call fill_spline_var (eloss_data(icharge))
                  call fill_spline_var (csx_data(icharge))
                  call fill_bspline_var(csx_data(icharge),4)
                  call fill_bspline_var(eloss_data(icharge),2)

                end do !icharge


        end subroutine


        subroutine fill_spline_var(dat)
                type(ion_data) :: dat
                integer        :: i, channum

                channum = size(dat%y,2)

                do i = 1, channum

                   call dat%sp(i)%initialize(dat%x,dat%y(:,i))

                end do

        end subroutine


        subroutine fill_bspline_var(dat,order)
                type(ion_data) :: dat
                integer        :: i, channum
                integer        :: iflag
                integer        :: idx = 0
                integer        :: order

                channum = size(dat%y,2)

                dat%bs_order = order

                do i = 1, channum

                    call dat%bs(i)%initialize(dat%x,dat%y(:,i),order,iflag)

                end do

        end subroutine


end module
