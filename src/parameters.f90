module parameters
  implicit none
  !private
  ! public   dp, &
  !          pi, &
  !          chan_num, &
  !          charge_num, &
  ! 		     min_charge, &
  !          energylist, &
  !          energy_num, &
  !          subchan_num, &
  !          ntraj, atm_num, &
  !          conv_fac, exc_enr, densnum, &
  !          upperlength, chem_sym, num_charge_channels


  integer , parameter :: dp=kind(0.d0)                   ! double precision
  !!!+++++++++++++++++++++++important parameters to set for every system+++++++++++++++++++++++++++++++++
  integer, parameter :: energy_num = 15 ! number of impact-energy points in the data tables (see energylist)
  !  integer, parameter :: chan_num = 9    ! number of main, non-simultaneous (NSIM), channels
  integer, parameter :: subchan_num = 1 ! number of sub-, simultaneous (SIM), channels

  integer, parameter :: charge_num = 1  ! highest ion charge
  integer, parameter :: min_charge = -1 ! lowest ion charge
  integer, parameter :: num_charge_channels = charge_num-min_charge+1 !

  integer, parameter :: chan_num(num_charge_channels) =[6,8,7]! [5,7,6]

  integer, parameter :: ntraj = 10000   ! number of trajectories
  integer, parameter :: atm_num = 1    ! atomic mass number of the projectile (O-->16)
  real(dp),parameter :: densnum = 2.67d19 !density number of the target
  real(dp),parameter :: upperlength = 1.d7

  character (len=1)  :: chem_sym    = 'h'
  !!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  real(dp), parameter, dimension (energy_num) :: energylist = [1.0_dp,2.0_dp,5.0_dp,10.0_dp,25.0_dp,50.0_dp,&
    75.0_dp,1.0e2_dp,2.0e2_dp,5.0e2_dp,&
    1.0e3_dp,2.0e3_dp,5.0e3_dp,&
    1.0e4_dp,2.5e4_dp]

  real(dp),parameter :: pi =  atan(1.0_dp)* 4
 real(dp), parameter :: avogadro_num=6.0221409d23
  real(dp),parameter :: conv_fac = Real(atm_num)*1.d3
  real(dp),parameter :: exc_enr= 7.7d0

  character (len=13) :: inputfolder = '../inputdata/'
  character (len=14) :: outputfolder = '../outputdata/'

  !!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
contains
  !*****************************************
  ! given charge gives index from 1:max
  !****************************************
  function charge_indx(charge)
    integer :: charge_indx
    integer :: charge

    charge_indx = charge - min_charge + 1

  end function
  !****************************************
  ! given index shows the charge
  !****************************************
  function indx_charge(icharge)
    integer :: icharge, indx_charge

    indx_charge = icharge + min_charge - 1

  end function

end module
