module monte_carlo
use parameters
use dataprocessor
use interpolation, only: intp_constants,spline_interpolate , line_interpolate
implicit none
private
public     stopping_power

real(dp) :: stopenergy
type channel_bin

   real(dp) :: bin(chan_num,subchan_num)

end type channel_bin

type(channel_bin),allocatable  :: chanbin(:,:) !min_charge:charge_num
integer                             :: chargeplus9
character (len=128)                 :: fmt1,fmt2
character (len=3), dimension(11)    :: chrarray
character (len=4), dimension(11)    :: chrarray10plus

contains
!***********************************************************************************************
subroutine random_walk(ion, length, level,flag)
!!====================================================================================
!!This subroutine will make a weighted-random choice between different channels available
!! to a given ion charge.
!!The weight is according to a Poisson distribution from each average cross section.
!!(it is preferenially on the smaller side.)
!!The new ion%channel and the associated ion%charge and ion%energy are assigned accordingly.
!!The length keeps the added travel lengths of the projectile in the medium.
!!flag determines if the lower energy, the maximum length or a given stopping point 
!!of interest is reached. flag==1 signals the termination of the Monte Carlo process.  
!!The difference with the previous routine is that it makes all the subprocesses
!compete against each other. 
!!==================================================================================== 
type(projectile_ion) :: ion
integer  :: i, j, k
integer  :: main_size
integer  :: sub_size
integer  :: level
integer  :: flag  
integer  :: minarray(2)
integer  :: initial_charge
integer  :: mindex_main, mindex_sub
integer  :: main_order, sub_order

real(dp) :: cross_section
real(dp) :: minlambda
real(dp) :: del
real(dp) :: length
real(dp) :: initial_energy
real(dp) :: distrib
real(dp), allocatable :: lambda(:,:) ! mean free paths
character(len=2) :: main_symbol, sub_symbol

!!  Save the incoming ion charge & energy
    initial_charge = ion%charge    
 
!!  Starting with main_processes find mean_free_path of available cross sections
    main_size = size(ion%main_process)!; print*,main_size
!! determine the subprocess:
      sub_size = size(ion%main_process(1)%sub_process)!;print*,sub_size
      allocate(lambda(sub_size,main_size))

    do i=1,main_size
      do j=1,sub_size
       cross_section = ion%main_process(i)%sub_process(j)%csx!; print*,cross_section
       distrib = rnunf()
!	 print*, 'distrib:',distrib, 'cross_section:',cross_section
       lambda(j,i) = -log(1.d0-distrib)/densnum/cross_section
      end do !j
    end do !i
!! choose minimum mean_free_path
    ! print*,lambda
    minlambda=minval(lambda)!; print*,minlambda
    minarray(:)=minloc(lambda)!; print*,minarray     !result of minloc is an array of rank 1
    mindex_main=minarray(2)!;print*,mindex_main
    mindex_sub=minarray(1)!;print*,mindex_sub


!it has to be put to rank zero for case statment
!	print*, 'lambda',lambda
!    print*, 'minlambda',minlambda
!! determine the symbol and order# of chosen main_process 	

    main_symbol = ion%main_process(mindex_main)%symbol
    main_order = ion%main_process(mindex_main)%order
    
!! select the main process and change ion charge accordingly	
    select case (main_symbol)
      case('SI', 'DI', 'EX','SP')
! nothing needs to be done charge is unchanged		
      case('TI', 'DA','SC' )
        ion%charge=ion%charge-1
      case('DC')
        ion%charge=ion%charge-2
       case('SS' )
          ion%charge=ion%charge+1
       case('DS')
          ion%charge=ion%charge+2
      case default
      !error
      print*, main_symbol
      stop 'main process symbol is not correct'
    end select

!! determine the symbol and order# of the chosen sub_process 	

    sub_symbol = ion%main_process(mindex_main)%sub_process(mindex_sub)%symbol

    sub_order  = ion%main_process(mindex_main)%sub_process(mindex_sub)%order

!!  determine the energy loss of the projectile for the sub_process	
    del = ion%main_process(mindex_main)%sub_process(mindex_sub)%enr_loss

!!	select the sub_process case and change ion charge accordingly
    ! select case (sub_symbol)
       ! case('PU', 'SP', 'DP')
      ! ! charge is unchanged
       ! case('SS' )
          ! ion%charge=ion%charge+1
       ! case('DS')
          ! ion%charge=ion%charge+2
       ! case default
       ! !error
        ! print*, sub_symbol
        ! stop 'sub process symbol is not correct'
    ! end select
if(ion%charge < min_charge .or. ion%charge>charge_num) stop 'charge exceeded maximum or went below zero'
!	print*, sub_symbol
!
chanbin(initial_charge,level)%bin(main_order,sub_order)=chanbin(initial_charge,level)%bin(main_order,sub_order)+1
     ! if(ion%charge<charge_num)then
     ! write(*,*)ion%charge, main_symbol, sub_symbol
     ! !stop
     ! end if	
!if(chanbin(main_order,sub_order,0)/=0)print*,main_order,sub_order,chanbin(main_order,sub_order,0)!; stop
length=length+minlambda



  ! print*,"del",del,"length",length,"minlambda",minlambda,"stopenergy",stopenergy
  ! print*,main_symbol,sub_symbol


 !print*,"del",del,"length",length,"minlambda",minlambda,"stopenergy",stopenergy
! print*,"flag",flag
!write(*,*)"length=",length,"cm"; if(intcount>40)stop

initial_energy=ion%energy*conv_fac!**IE in the tables is normalized by atomic number
ion%energy=(initial_energy-del)/conv_fac
! print*,"upperlength",upperlength, "energy",ion%energy
if(length>=upperlength.OR.ion%energy<=stopenergy.OR.ion%energy<=0.d0)flag=1
! print*,"flag",flag;print*,"********************"
deallocate(lambda)

end subroutine random_walk
!***********************************************************************************************

subroutine stopping_power(start_E)
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!A set of energy intervals are read from the text file intv.txt
!!For the given intervals the average of stopping power is calculated
!!across all the starting charges (q=0-charge_num).
!!The equilibrium of stopping is achieved when all starting charges result in
!! more or less the same stopping power. The results are documented in the file
!! SP_intv.out along with the average of stopping for each given energy interval
!!and the standard deviation of the stopping powers for each starting charge-state.
!!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
integer::i,j,k,l,jmax,flag,num_interaction(ntraj),start_charge, last_charge
integer::io, nlines,sum_charge
integer::ncounted
real::placeholder
real,dimension(:),allocatable::intv
real(dp),dimension(:,:,:),allocatable::intv_length
real(dp),dimension(:,:),allocatable::stoppwr,lavg,charge_bin_percent, chansum
real(dp),dimension(:),allocatable::mean,sig, del_e_MeVcm2_over_mg,avg_l
real(dp),dimension(:,:),allocatable::charge_bin
real(dp):: length, sig_length,start_E, Equi_charge
character(len=2), dimension(chan_num)::list
character(len=2), dimension(subchan_num)::sublist
type(projectile_ion) :: ion

chrarray = ['q=-','q=0','q=1','q=2','q=3','q=4','q=5','q=6','q=7','q=8','q=9']
chrarray10plus = ['q=10','q=11','q=12','q=13','q=14','q=15','q=16','q=17','q=18','q=19','q=20']
      
      write(*,*)"Initial Energy: ", start_E, " KeV/u"
      
  !**determines the intervals from file
  !first reads the length of the relevant intervals
       i= 0; j= 0; io= 0 
      open(unit = 3,file ="intv.txt",status ='OLD',iostat =io)
      do while(io == 0)
         read(3,*, iostat = io)placeholder
         j=j+1!; print*,j
      end do
      
!print*,io
  rewind(unit = 3, iostat = io)
    !print*, io

  nlines=j-1   !;print*,"end line:",nlines
  
  
  
       do j=1,nlines!27
         read(3,*, iostat = io)placeholder 
         i=i+1
         if(start_E-placeholder.lt.1.d0)then
          i=i-1
          exit
         endif
       end do
      rewind(unit = 3, iostat =io)
  
  !**number of relevant energy intervals to be used 
      jmax=i
      print*,"jmax:",jmax

      allocate(intv(jmax), intv_length(jmax,ntraj,-1:charge_num),stoppwr(jmax,-1:charge_num))
      allocate(mean(jmax),sig(jmax),lavg(jmax,-1:charge_num))
      allocate(charge_bin(jmax,-1:charge_num),charge_bin_percent(jmax,-1:charge_num)) !!** bining of the final charge percentages
      allocate(chanbin(min_charge:charge_num,jmax),chansum(min_charge:charge_num,jmax))
	  allocate(del_e_MeVcm2_over_mg(0:jmax+1),avg_l(0:jmax+1))
	  open(unit=3,file="intv.txt",status='OLD')
  !************************************************
       !**initializing counters 
          
          charge_bin = 0
          charge_bin_percent = 0d0
          stoppwr = 0.d0
          lavg = 0.d0
          sig = 0.d0
          mean= 0.d0
          Equi_charge=0.0
  !***********************************************
       do j=1,jmax
         read(3,*,iostat = io)intv(j)
   !print*,"intv(",j,"):",intv(j)
       end do  
      close(3)
  

  !**determines energy to stop the simulation 
      stopenergy=start_E-intv(jmax)
     !print*,energy,stopenergy
       
 
      if(stopenergy<1.d0 )then
          write(*,*)"Intervals are too large for given energy"
          stop
      end if
  
      !**Loop over ion charges
      do l=-1,charge_num
        start_charge= l; print*,'start_charge',start_charge
        sig_length=0.d0
        num_interaction=0
        intv_length=0.d0
  

!**loop over different trajectories
        do k=1,ntraj
           ion%energy=start_E
           ion%charge=start_charge
           length=0.d0
           flag=0
 !*****************************************************************
!***Start with the first interval
         j=1
 chanbin(l,j)%bin=0.d0
!         i=0
!***Loop over interactions until the end/stop flag (flag==1) is raised
         do while (flag == 0 )
!**interpolate to find cross section and energy losses for new impact energy
 !*************************************************************************
             call channel_allocation(ion)
 if(ion%energy<=energylist(energy_num-1) .and.ion%energy>=energylist(2) ) then
                call spline_interpolate(ion)
 else
                call line_interpolate(ion)
 end if
!*************************************************************************
!**according to the charge (channel #) select the appropriate routine
!**to recalculate new energy and length
!*************************************************************************

            call random_walk(ion,length,j,flag)
            if(ion%energy.LE.start_E-intv(j))then !save length at the end of each intv
               intv_length(j,k,l)=length
               charge_bin(j,ion%charge) = charge_bin(j,ion%charge) + 1
               j=j+1 !going to the next interval
            end if
   
            call channel_deallocation(ion)
!             i=i+1
!			if(ion%charge<charge_num)then
!             print*,i, ion%charge, ion%energy, length; stop
!			end if 
         end do !interactions
     
 
    !num_interaction(k)=i-1

       end do!trajectories				
  
       ncounted = ntraj
  
       do j=1,jmax
          sig_length=0.d0
          sum_charge = 0
          do i=-1,charge_num
             sum_charge = sum_charge + charge_bin(j,i)
          end do  
          charge_bin_percent(j,:) = charge_bin(j,:)/dble(sum_charge)*100.d0
          do k=1,ntraj
             ! if(l==0)then
             ! print*,intv_length(j,k,l)
             ! endif
             sig_length=sig_length+intv_length(j,k,l)
			 if(intv_length(j,k,l)==0.d0)then
			 
			    ncounted = ncounted - 1
			 
			 end if
          end do
!** calculate stopping power
             
            lavg(j,l)=sig_length/ncounted
            !print*,sig_length,lavg(j,l)			
            stoppwr(j,l)=Real(atm_num)*intv(j)/1000.0/(lavg(j,l))/&
                                            (2.0d3*densnum/6.0221409d23)
            
      end do
  
  
    
    end do !l	

      sig=0.d0
      !saves data
     open(unit= 24, file="SP_intv.out")
	 open(unit=25,file='channeldata.dat')
     open(unit= 21, file="Curve.out")
        write(24,*)'stopping powers in Mev.cm^2/mg for charges:'
        write(24,'(A100)')'======================================================================================================='
        write(24,'(A11,x,5(2x,A6))')'dE(KeV/u)','q=-','q=0','q=1','Avg','sigma'
        write(24,'(A100)')'======================================================================================================='
       
	   del_e_MeVcm2_over_mg(0)=0.d0
	   avg_l(0) = 0.d0
	   
	   do j=1,jmax
	       del_e_MeVcm2_over_mg(j)=Real(atm_num)*(start_E-intv(j))/1.d3/(2.0d3*densnum/6.0221409d23)
		   !!print*, sum(lavg(j,:)),lavg(j,-1)+lavg(j,0)+lavg(j,1)
	       avg_l(j)=sum(lavg(j,:))/real(num_charge_channels)
	   end do
	   del_e_MeVcm2_over_mg(jmax+1)=0.d0
	   avg_l(jmax+1) = 0.d0
	   
	   
	   do j=1,jmax
              mean(j)=sum(stoppwr(j,:))/(charge_num+2)
             do i= -1, charge_num
               sig(j)=sig(j)+(stoppwr(j,i)-mean(j))**2
  
             end do
			 
              sig(j)=sqrt(sig(j)/(charge_num+1))
              write(24,'(F6.2,6x,5(2x,F6.2))')intv(j),stoppwr(j,:),mean(j),sig(j)
              !print*,mean(j), sig(j)
              write(21,'(9F10.5,F6.3, 3F7.2)')lavg(j,:),avg_l(j),(start_E-intv(j)),&
              !print'(4F10.5,F6.3, F7.2)',lavg(j,:),(start_E-intv(j))/1.d3,&
  &       del_e_MeVcm2_over_mg(j), (del_e_MeVcm2_over_mg(j)-del_e_MeVcm2_over_mg(j-1))/(avg_l(j)-avg_l(j-1)),&
  &       (del_e_MeVcm2_over_mg(j)-del_e_MeVcm2_over_mg(j+1))/(avg_l(j)-avg_l(j+1))  
       enddo


  ! do i= 0, charge_num
      ! print*,intv(7),stoppwr(7,i),stoppwr(7,i)-mean(7),(stoppwr(7,i)-mean(7))**2
  ! end do
   ! print*,sum((stoppwr(7,:)-mean(7))**2), sum((stoppwr(7,:)-mean(7))**2)/charge_num,sqrt(sum((stoppwr(7,:)-mean(7))**2)/charge_num)
 chansum = 0.d0
  do l= 1, jmax
    do k= -1, charge_num
       do j =1, subchan_num
          do i= 1,chan_num
     
             chansum(k,l)=chanbin(k,l)%bin(i,j)+chansum(k,l)
	 
          end do
       end do
    end do
  end do  

   !print*,chansum(0)

        list = ['SI','DI','TI','SS','DS','SC','DC','EX','SP']
        sublist = ['PU']
        write(25,*)'percentages of interaction channels for oxygen on hydrogen:'
        write(25,'(A95)')'============================================================================================'
        write(25,'(x,A6,x,A11,3(2x,A6))')'Energy','interaction','q=-','q=0','q=1'
        write(25,'(A95)')'============================================================================================='
		do l = 1, jmax
          do i=1,chan_num
             do j = 1, subchan_num
             write(25,'(F7.2,4x,A2,":",A2,4x,3(2x,F6.1))')start_E-intv(l),list(i),sublist(j),&
    &            chanbin(-1,l)%bin(i,j)*100.0/real(chansum(-1,l)),&
    &            chanbin(0,l)%bin(i,j)*100.0/real(chansum(0,l)),&
    &            chanbin(1,l)%bin(i,j)*100.0/real(chansum(1,l))!,&
    ! &            chanbin(2,l)%bin(i,j)*100.0/real(chansum(2,l)),&
    ! &            chanbin(3,l)%bin(i,j)*100.0/real(chansum(3,l)),&
    ! &            chanbin(4,l)%bin(i,j)*100.0/real(chansum(4,l)),&
    ! &            chanbin(5,l)%bin(i,j)*100.0/real(chansum(5,l)),&
    ! &            chanbin(6,l)%bin(i,j)*100.0/real(chansum(6,l)),&
    ! &            chanbin(7,l)%bin(i,j)*100.0/real(chansum(7,l)),&
    ! &            chanbin(8,l)%bin(i,j)*100.0/real(chansum(8,l))	
             end do
          end do
        end do
     write(24,*)'percentages of charges at the end of each interval:'
     write(24,'(A95)')'==============================================================================================='
     write(24,'(A10,x,10(2x,A6),2x,A3)')'int(kev/u)','q=-','q=0','q=1'
     ! if (charge_num < 10) then
           ! write(fmt1,'("(A11,x,",I1,"(2x,A6),2(2x,A6))")')charge_num
           ! write(24,fmt1)'dE(KeV/u)',(chrarray(i),i=1,charge_num+2),'Avg','sigma'
     ! else
       ! chargeplus9 = charge_num - 9
           ! write(fmt2,'("(A11,x,10(2x,A6)",I2,"(2x,A7),2(2x,A6))")')chargeplus9
           ! write(24,fmt2)'dE(KeV/u)',(chrarray(i),i=1,10),(chrarray10plus(i),i=1,chargeplus9),'Avg','sigma'
     ! endif
     write(24,'(A95)')'===============================================================================================' 
     do j=1,jmax
       Equi_charge=0
       do i=-1,charge_num
          Equi_charge=Equi_charge+charge_bin_percent(j,i)*dble(i) 
       end do
       Equi_charge=Equi_charge/100.0
	   !write(*,*)intv(j), charge_bin_percent(j,:), Equi_charge
       write(24,'(2x,F5.2,4x,3(2x,F6.1),2x,F7.2)') intv(j), charge_bin_percent(j,:), Equi_charge

     end do
  
 ! do i=0,charge_num
 ! Equi_charge=Equi_charge+charge_bin(jmax,i)*real(i) 
 ! end do
 ! Equi_charge=Equi_charge/100.0
 ! print*,'Equilibrium charge at Jmax:',Equi_charge
  
      deallocate(intv,intv_length,stoppwr, mean, sig,lavg,charge_bin)
      close(24);close(21)

end subroutine stopping_power      
!****************************************************************************	  
      function rnunf()
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
! Random number generator routine after the ran2 recipe of numerical recipes.
!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      INTEGER idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,NTAB,NDIV
      DOUBLE PRECISION rnunf,AM,EPS,RNMX
      PARAMETER (IM1=2147483563,IM2=2147483399,AM=1.d0/IM1,IMM1=IM1-1,&
     &IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,IR2=3791,&
     &NTAB=32,NDIV=1+IMM1/NTAB,EPS=3.d-16,RNMX=1.d0-EPS)
      INTEGER idum2,j,k,iv(NTAB),iy
      common /rands/ idum
!      common /randsave/	nrnum  
      SAVE iv,iy,idum2
      DATA idum2/123456789/, iv/NTAB*0/, iy/0/
      if (idum.le.0) then
        idum=max(-idum,1)
        idum2=idum
        do 11 j=NTAB+8,1,-1
          k=idum/IQ1
          idum=IA1*(idum-k*IQ1)-k*IR1
          if (idum.lt.0) idum=idum+IM1
          if (j.le.NTAB) iv(j)=idum
11      continue
        iy=iv(1)
      endif
      k=idum/IQ1
      idum=IA1*(idum-k*IQ1)-k*IR1
      if (idum.lt.0) idum=idum+IM1
      k=idum2/IQ2
      idum2=IA2*(idum2-k*IQ2)-k*IR2
      if (idum2.lt.0) idum2=idum2+IM2
      j=1+iy/NDIV
      iy=iv(j)-idum2
      iv(j)=idum
      if(iy.lt.1)iy=iy+IMM1
      rnunf=min(AM*iy,RNMX)
!	     nrnum=nrnum+1

      return
     End function
end module
